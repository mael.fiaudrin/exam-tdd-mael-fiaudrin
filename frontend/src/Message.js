class Message {
    constructor(content, author) {
        if (content.length < 2 || content.length > 2048) {
            this.error = 'Le message doit faire entre 2 et 2048 caractères.';
            return ;
        }
        this.content = content;
        this.author = author;
        this.date = Date.now();
    }

}
module.exports = Message;