const Message = require('./Message');

const allSalons = [];

class Salon {
    constructor(name) {
        if(!allSalons.includes(name)){
            allSalons.push(name)
            this.name = name;
            this.messages = [];
        } else {
            this.error = "Le salon existe déjà.";
        }
    }

    ajouterMessage(content, author) {
        const dateNow = Date.now();
        const message = new Message(content, author);
        if (this.messages.length > 0 && this.messages[this.messages.length-1].author === author &&
            dateNow - this.messages[this.messages.length-1].date < 24 * 60 * 60 * 1000) {
                return 'Vous ne pouvez pas poster deux messages consécutifs.';
        }
        if(message.error == 'Le message doit faire entre 2 et 2048 caractères.'){
            return 'Le message doit faire entre 2 et 2048 caractères.';
        }
        this.messages.push(message);
    }
    
    getMessages() {
        return this.messages.sort((a, b) => a.date - b.date);
    }


}
module.exports = Salon;