const Message = require('../src/Message');

describe("Unit tests for class Message", () => {

    test('initialisation d\'un message', () => {
        const message = new Message('Message de test', 'Utilisateur de test');
        expect(message.content).toBe('Message de test');
        expect(message.author).toBe('Utilisateur de test');
        expect(message.error).toBeUndefined();
    });

    test('initialisation d\'un message ayant moins de 2 caractères', () => {
        const message = new Message('', 'Utilisateur de test')
        expect(message.content).toBeUndefined()
        expect(message.author).toBeUndefined()
        expect(message.error).toBe('Le message doit faire entre 2 et 2048 caractères.')
    });

    test('initialisation d\'un message ayant plus de 2 caractères', () => {
        const message = new Message('a'.repeat(2049), 'Utilisateur de test')
        expect(message.content).toBeUndefined()
        expect(message.author).toBeUndefined()
        expect(message.error).toBe('Le message doit faire entre 2 et 2048 caractères.')
    });
});
