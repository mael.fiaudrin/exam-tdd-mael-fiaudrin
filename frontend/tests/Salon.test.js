const Salon = require('../src/Salon');

describe('Unit tests for class Salon', () => {
    const salon = new Salon('Nom du salon');
    test('création d\'un salon', () => {
      expect(salon.name).toBe('Nom du salon');
      expect(salon.messages.length).toBe(0);
    });
  
    test('ajout d\'un message dans le salon', () => {
      salon.ajouterMessage('Premier message', 'Utilisateur de test');
      expect(salon.messages.length).toBe(1);
      expect(salon.messages[salon.messages.length-1].content).toBe('Premier message');
      expect(salon.messages[salon.messages.length-1].author).toBe('Utilisateur de test');
    });
  
    test('interdiction d\'ajout de messages consécutifs en moins de 24 heures', () => {
      expect(salon.ajouterMessage('Deuxième message', 'Utilisateur de test'))
        .toBe('Vous ne pouvez pas poster deux messages consécutifs.');
    });
  
    test('ajout de message après 24 heures', () => {
      salon.messages[0].date = Date.now() - 25 * 60 * 60 * 1000
      salon.ajouterMessage('Deuxième message', 'Utilisateur de test');
      expect(salon.messages.length).toBe(2);
    });

    test('ajout d\'un message d\'un autre utilisateur dans le salon', () => {
        salon.ajouterMessage('Troisième message', 'Autre utilisateur');

        expect(salon.messages.length).toBe(3);
        expect(salon.messages[salon.messages.length-1].content).toBe('Troisième message');
        expect(salon.messages[salon.messages.length-1].author).toBe('Autre utilisateur');
      });
  
    test('interdiction de message vide ou trop long', () => {
      expect( salon.ajouterMessage('', 'Utilisateur de test'))
        .toBe('Le message doit faire entre 2 et 2048 caractères.');
      expect( salon.ajouterMessage('a'.repeat(2049), 'Utilisateur de test'))
        .toBe('Le message doit faire entre 2 et 2048 caractères.');
    });

    test('interdiction de création de salon déjà existant', () => {
      const salonWithSameName = new Salon('Nom du salon');
      expect( salonWithSameName.error)
        .toBe("Le salon existe déjà.");
    });
    
    test('création d\'un nouveau salon', () => {
      const salon2 = new Salon('Nom du salon 2');
      expect( salon2.name)
        .toBe("Nom du salon 2");
    });

});