# Exam Tdd Mael Fiaudrin

## Présentation du projet
### Contexte
Ce projet est un site de type message board. Depuis ce site, les utilisateurs peuvent créer un salon en lui attribuant un nom.

Ils peuvent également rejoindre un salon pour accéder aux messages de ce salon.

Les salons ont des messages et ils sont visibles par les utilisateurs.

### Règles
- Un utilisateur ne peut pas créer un salon avec un nom déjà utilisé. 
- Lorsqu'un utilisateur est dans un salon, il peut ajouter un message à condition que ce message fasse au moins 2 caractères et au maximum 2048 caractères.
- Un utilisateur ne peut pas envoyer deux messages d'affilés en moins de 24 heures

### Etat actuel du développement
Actuellement, le site n'est pas accessible.

Seul les tests (unitiares et fonctionnels) ainsi que les classes prennant en compte les règles sont implémentés.

Le développement a été réalisé en TDD et tous les tests fonctionnent.

### Architecture
L'architecture du site est la suivante :
- backend
- frontend
- public

Dans le dossier `frontend`, nous avons :
- les classes js Message et Salon dans le dossier `src`
- les tests unitaires en Jest dans le dossier `tests`

Dans le dossier `backend`, nous avons : 
- les classes php Message et Salon dans le dossier `src`
- les tests unitaires en PHPUnit dans le dossier `tests`
- les tests fonctionnels en Behat dans le dossier `features`


## Lancer les tests unitaires avec Jest
- Pour lancer les tests unitaires en js avec Jest, dirigez vous dans le dossier `frontend`
- Faites un `npm install`
- Lancez la commande `npm test` pour lancer les tests unitaires des classes issues du dossier `frontend/tests`

## Lancer les tests unitaires avec PHPUnit
- Pour lancer les tests unitaire avec PHPUnit, dirigez vous dans le dossier `backend`
- Faites un `composer update` pour mettre à jour composer
- Lancez la commande `./vendor/bin/phpunit tests` pour lancer les tests unitaires des classes issues du dossier `backend/tests`

## Lancer les tests fonctionnels avec behat
- Pour lancer les tests fonctionnels avec behat, dirigez vous dans le dossier `backend`
- Lancez la commande `vendor/bin/behat` pour lancer les tests fonctionnels des classes issues du dossier `backend/features/bootstrap/FeatureContext.php`

