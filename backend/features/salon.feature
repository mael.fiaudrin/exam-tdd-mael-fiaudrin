Feature: Messages in salons
  In order to send messages in salons
  As a user
  I need to be able to create a salon and send messages inside

  Rules:
  - Should know who send message
  - Can send message in every salons
  - Can't send 2 messages in a row in less than 24 hours
  - Can't send message lower than 2 characters
  - Can't send message higher than 2048 characters
  - Can't create salon with name already existing
  - Should read messages sorted from oldest to newest

  Scenario: Can create a room
    Given User create room "room 1"
    Then Name of room should be "room 1"

  Scenario: Send a message in a room
    Given User create room "room 1"
    When User "Mael" send message "Hello world" in room
    Then Should have 1 messages in room
    And Last message in room should be "Hello world"
    And User who send last message in room should be "Mael"

  Scenario: Send two messages in less than 24 hours
    Given User create room "room 1"
    When User "Mael" send message "First message" in room
    And User "Mael" send message "Second message" in room
    Then Should have 1 messages in room
    And Message error should be "Vous ne pouvez pas poster deux messages consécutifs."

  Scenario: Send two messages in more than 1 day
    Given User create room "room 1"
    When User "Mael" send message "Premier message" in room
    And User "Mael" send message "Second message" in room after a day
    Then Should have 2 messages in room

  Scenario: Other user send message in same room
    Given User create room "room 1"
    When User "Mael" send message "Hello world" in room
    And User "Mathilde" send message "Hola mundo" in room
    Then Should have 2 messages in room
    And User who send last message in room should be "Mathilde"
    And Last message in room should be "Hola mundo"

  Scenario: Send a message lower than 2 characters
    Given User create room "room 1"
    When User "Mael" send message "" in room
    Then Should have 0 messages in room
    And Message error should be "Le message doit faire entre 2 et 2048 caractères."

  Scenario: Send a message higher than 2 characters
    Given User create room "room 1"
    When User "Mael" send message "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus sit amet turpis nibh. Aliquam interdum ex non est iaculis blandit. Nulla facilisi. Vivamus sit amet risus sed augue vehicula ornare quis vitae mauris. Ut tristique dictum turpis. Duis congue a neque at gravida. In tempus luctus eros, non molestie velit bibendum ut. Aenean porttitor velit placerat facilisis scelerisque. Proin eu urna mi. Vivamus volutpat ornare risus, eu venenatis metus hendrerit ac. Vivamus a sapien diam. Cras eget ultricies arcu. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Cras sit amet facilisis nulla. Donec quis lacinia quam, quis placerat justo.Sed vel odio arcu. Nunc vehicula risus leo, id ultricies lacus tempor eget. Mauris eleifend mauris a velit pretium, id egestas felis sollicitudin. Ut fringilla tellus eu velit congue rutrum. Cras aliquet dui purus, at fringilla tortor ultricies nec. Aenean faucibus, magna et tincidunt lobortis, enim elit rutrum arcu, non condimentum felis justo quis quam. Ut arcu arcu, semper quis diam ut, imperdiet eleifend nunc. Aliquam congue rutrum pharetra. Maecenas in elit purus. Nunc varius mollis quam, sed dignissim diam condimentum vel. Pellentesque convallis erat vitae imperdiet iaculis. Mauris eget diam et tortor egestas sodales a vestibulum diam. Duis vestibulum libero ac dignissim consectetur. Proin metus felis, cursus quis cursus nec, congue a eros. Etiam at porttitor ante.Donec vel lacus eu arcu faucibus commodo sed eu tortor. Nam bibendum, lectus et dapibus malesuada, urna enim fringilla magna, et cursus felis magna nec dui. Duis quis sapien quis quam maximus faucibus sit amet tincidunt sapien. Vestibulum urna quam, posuere ac pharetra id, commodo tincidunt turpis. Vivamus semper, arcu et faucibus egestas, dolor metus pellentesque diam, ac interdum enim nunc sed magna. Nunc condimentum aliquam orci quis porta. Phasellus scelerisque nisi sed maximus varius. Donec convallis tellus metus, nec iaculis urna consequat non. Praesent nisl arcu turpis duis." in room
    Then Should have 0 messages in room
    And Message error should be "Le message doit faire entre 2 et 2048 caractères."

  Scenario: Can't create a room with name already existing
    Given User create room "room 1"
    When User create room "room 1"
    Then Salon error should be "Le salon existe déjà."
