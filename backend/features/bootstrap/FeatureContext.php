<?php

use Behat\Behat\Tester\Exception\PendingException;
use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use PHPUnit\Framework\Assert;

require "src/Message.php";  
require "src/Salon.php";

/**
 * Defines application features from the specific context.
 */
class FeatureContext implements Context
{

	private Salon $salon;
	private $errorSalon;
	private $errorMessage;


    /**
     * @Given User create room :name
     */
    public function userCreateRoom($name)
    {
        $this->salon = new Salon($name);
        $this->errorSalon = $this->salon;
    }

    /**
     * @Then Name of room should be :name
     */
    public function nameOfRoomShouldBe($name)
    {
        Assert::assertSame(
            $name,
            $this->salon->name
        );
    }

    /**
     * @When User :user send message :message in room
     */
    public function userSendMessageInRoom($user, $message)
    {
        $this->salon->ajouterMessage($message, $user);
        if($this->salon->error){
            $this->errorMessage = $this->salon->error;
        }
    }

    /**
     * @Then Should have :count messages in room
     */
    public function shouldHaveMessagesInRoom($count)
    {
        Assert::assertCount(
            intval($count),
            $this->salon->messages
        );
    }

    /**
     * @Then Last message in room should be :message
     */
    public function lastMessageInRoomShouldBe($message)
    {
        Assert::assertSame(
            $message,
            $this->salon->messages[count($this->salon->messages)-1]->content
        );
    }

    /**
     * @Then User who send last message in room should be :user
     */
    public function userWhoSendLastMessageInRoomShouldBe($user)
    {
        Assert::assertSame(
            $user,
            $this->salon->messages[count($this->salon->messages)-1]->author
        );
    }

    /**
     * @Then Message error should be :error
     */
    public function messageErrorShouldBe($error)
    {
        Assert::assertSame(
            $error,
            $this->errorMessage
        );
    }

    /**
     * @When user :user send message :message in room after a day
     */
    public function userSendMessageInRoomAfterADay($user, $message)
    {
        $this->salon->messages[count($this->salon->messages)-1]->date = time() - 25 * 60 * 60 * 1000;
        $this->salon->ajouterMessage($message, $user);
    }

    /**
     * @Then Salon error should be :error
     */
    public function salonErrorShouldBe($error)
    {
        Assert::assertSame(
            $error,
            $this->errorSalon->error
        );
    }

}
