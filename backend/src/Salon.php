<?php

require_once 'Message.php';

class Salon {
    public static $allSalons = array();

    public $name;
    public $messages;

    public function __construct($name) {
        if (!in_array($name, self::$allSalons)) {
            self::$allSalons[] = $name;
            $this->name = $name;
            $this->messages = array();
        } else {
            $this->error = "Le salon existe déjà.";
        }
    }

    public function ajouterMessage($content, $author) {
        $dateNow = time();
        $message = new Message($content, $author);
        if (!is_array($this->messages)) {
            $this->messages = array();
        }
        if (!empty($this->messages) && $this->messages[count($this->messages) - 1]->author === $author &&
            $dateNow - $this->messages[count($this->messages) - 1]->date < 24 * 60 * 60 * 1000) {
            $this->error = "Vous ne pouvez pas poster deux messages consécutifs.";
            return 'Vous ne pouvez pas poster deux messages consécutifs.';
        }
        if ($message->error === 'Le message doit faire entre 2 et 2048 caractères.') {
            $this->error = "Le message doit faire entre 2 et 2048 caractères.";
            return 'Le message doit faire entre 2 et 2048 caractères.';
        }
        array_push($this->messages, $message);
    }

    public function getMessages() {
        usort($this->messages, function($a, $b) {
            return $a->date - $b->date;
        });
        return $this->messages;
    }
}

?>
