<?php


class Message {
    public $content;
    public $author;
    public $date;
    public $error;

    public function __construct($content, $author) {
        if (strlen($content) < 2 || strlen($content) > 2048) {
            $this->error = 'Le message doit faire entre 2 et 2048 caractères.';
            return;
        }
        $this->content = $content;
        $this->author = $author;
        $this->date = time();
    }
}

?>