<?php

class MessageTest extends \PHPUnit\Framework\TestCase {

    public function testConstructor() {
        // Test constructor with valid content and author
        $message = new Message('Hello, world!', 'John');
        $this->assertInstanceOf(Message::class, $message);
        $this->assertEquals('Hello, world!', $message->content);
        $this->assertEquals('John', $message->author);
        $this->assertGreaterThanOrEqual(time(), $message->date);

        // Test constructor with invalid content
        $message = new Message('H', 'John');
        $this->assertInstanceOf(Message::class, $message);
        $this->assertNull($message->content);
        $this->assertEquals('Le message doit faire entre 2 et 2048 caractères.', $message->error);
        $this->assertNull($message->author);
        $this->assertNull($message->date);

        // Test constructor with empty content
        $message = new Message('', 'John');
        $this->assertInstanceOf(Message::class, $message);
        $this->assertNull($message->content);
        $this->assertEquals('Le message doit faire entre 2 et 2048 caractères.', $message->error);
        $this->assertNull($message->author);
        $this->assertNull($message->date);
    }

}