<?php


class SalonTest extends \PHPUnit\Framework\TestCase {

    public function setUp(): void
    {
        parent::setUp();
        Salon::$allSalons = array(); // réinitialiser la variable $allSalons entre chaque test
    }

    public function testCreationSalon()
    {
        $salon = new Salon('Nom du salon');
        $this->assertEquals('Nom du salon', $salon->name);
        $this->assertEquals(0, count($salon->messages));
    }

    public function testAjoutMessage()
    {
        $salon = new Salon('Nom du salon');
        $salon->ajouterMessage('Premier message', 'Utilisateur de test');
        $this->assertEquals(1, count($salon->messages));
        $this->assertEquals('Premier message', $salon->messages[count($salon->messages)-1]->content);
        $this->assertEquals('Utilisateur de test', $salon->messages[count($salon->messages)-1]->author);
    }

    public function testInterdictionAjoutMessageConsecutif()
    {
        $salon = new Salon('Nom du salon');
        $salon->ajouterMessage('Premier message', 'Utilisateur de test');
        $result = $salon->ajouterMessage('Deuxième message', 'Utilisateur de test');
        $this->assertEquals('Vous ne pouvez pas poster deux messages consécutifs.', $result);
    }

    public function testAjoutMessageApres24h()
    {
        $salon = new Salon('Nom du salon');
        $salon->ajouterMessage('Premier message', 'Utilisateur de test');
        $salon->messages[count($salon->messages)-1]->date = time() - 25 * 60 * 60 * 1000;
        $salon->ajouterMessage('Deuxième message', 'Utilisateur de test');
        $this->assertEquals(2, count($salon->messages));
    }

    public function testAjoutMessageAutreUtilisateur()
    {
        $salon = new Salon('Nom du salon');
        $salon->ajouterMessage('Premier message', 'Utilisateur de test');
        $salon->ajouterMessage('Deuxième message', 'Autre utilisateur');
        $this->assertEquals(2, count($salon->messages));
        $this->assertEquals('Deuxième message', $salon->messages[count($salon->messages)-1]->content);
        $this->assertEquals('Autre utilisateur', $salon->messages[count($salon->messages)-1]->author);
    }

    public function testInterdictionMessageVideOuTropLong()
    {
        $salon = new Salon('Nom du salon');
        $result1 = $salon->ajouterMessage('', 'Utilisateur de test');
        $result2 = $salon->ajouterMessage(str_repeat('a', 2049), 'Utilisateur de test');
        $this->assertEquals('Le message doit faire entre 2 et 2048 caractères.', $result1);
        $this->assertEquals('Le message doit faire entre 2 et 2048 caractères.', $result2);
    }

    public function testInterdictionCreationSalonExistant()
    {
        $salon = new Salon('Nom du salon');
        $salonWithSameName = new Salon('Nom du salon');
        $this->assertEquals('Le salon existe déjà.', $salonWithSameName->error);
    }

    public function testCreationNouveauSalon()
    {
        $salon = new Salon('Nom du salon');
        $salon2 = new Salon('Nom du salon 2');
        $this->assertEquals('Nom du salon 2', $salon2->name);
    }
}

?>